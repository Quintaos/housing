package com.quintaos.housing;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.quintaos.housing.models.House;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        House house = intent.getParcelableExtra("HOUSE");

        CollapsingToolbarLayout toolbar_layout = findViewById(R.id.toolbar_layout);
        ImageView iv_housePic = findViewById(R.id.iv_housePic);
        TextView tv_title = findViewById(R.id.tv_title);
        TextView tv_keywords = findViewById(R.id.tv_keywords);
        TextView tv_bedroomNumber = findViewById(R.id.tv_bedroomNumber);
        TextView tv_bathroomNumber = findViewById(R.id.tv_bathroomNumber);
        TextView tv_summary = findViewById(R.id.tv_summary);

        toolbar_layout.setTitle(house.getPriceFormatted());

        Glide.with(this)
                .load(house.getImgUrl())
                .centerCrop()
                .into(iv_housePic);

        tv_title.setText(house.getTitle());
        tv_keywords.setText(house.getKeywords());
        tv_bedroomNumber.setText(house.getBedroomNumber() + " Bedroom(s)");
        tv_bathroomNumber.setText(house.getBathroomNumber() + " Bathroom(s)");
        tv_summary.setText(house.getSummary());

    }
}
