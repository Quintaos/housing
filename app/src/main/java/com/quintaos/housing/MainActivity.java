package com.quintaos.housing;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Toast;

import com.quintaos.housing.interfaces.ListInteractionListener;
import com.quintaos.housing.models.House;
import com.quintaos.housing.models.ResponseObject;
import com.quintaos.housing.network.NestoriaAPI;
import com.quintaos.housing.views.adapters.HousingListAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements ListInteractionListener {

    private NestoriaAPI retrofit;
    private HousingListAdapter[] mAdapter;
    private RecyclerView mRecyclerView;
    private int bedroomNumber = 0;
    private String maxPrice;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Define views
        retrofit = NestoriaAPI.retrofit.create(NestoriaAPI.class);
        mAdapter = new HousingListAdapter[1];
        mRecyclerView = findViewById(R.id.list);
        progressBar = findViewById(R.id.progressBar);
        final EditText et_priceFilter = findViewById(R.id.et_priceFilter);
        Button btn_go = findViewById(R.id.btn_go);

        // Remove transformation
        et_priceFilter.setTransformationMethod(null);
        // Hide keyboard on focus change
        et_priceFilter.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        // Create list Item divider
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(this.getResources().getDrawable(R.drawable.divider_item_decoration));

        // Configure RecyclerView
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        // Execute first call to fill the list
        apiCall(0);

        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(et_priceFilter);
                progressBar.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.INVISIBLE);
                maxPrice = et_priceFilter.getText().toString();
                // no Bedrooms, no price
                if (bedroomNumber == 0 && maxPrice.isEmpty()) {
                    apiCall(0);
                }
                // X Bedrooms, no price
                else if (bedroomNumber != 0 && maxPrice.isEmpty()) {
                    apiCall(1);
                }
                // no Bedrooms, Y price
                else if (bedroomNumber == 0 && !maxPrice.isEmpty()) {
                    apiCall(2);
                }
                // X Bedrooms, Y price
                else if (bedroomNumber != 0 && !maxPrice.isEmpty()) {
                    apiCall(3);
                }
            }
        });
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        assert inputMethodManager != null;
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void apiCall(int i) {
        switch (i) {
            case 1:
                retrofit.houseListBedroom(getString(R.string.city_name), bedroomNumber)
                        .enqueue(new Callback<ResponseObject>() {
                            @Override
                            public void onResponse(@NonNull Call<ResponseObject> call, @NonNull Response<ResponseObject> response) {
                                try {
                                    if (response.isSuccessful()) {
                                        List<House> houseList = response.body().getResponse().getListings();
                                        mAdapter[0] = new HousingListAdapter(houseList, MainActivity.this);
                                        mRecyclerView.setAdapter(mAdapter[0]);
                                        progressBar.setVisibility(View.INVISIBLE);
                                        mRecyclerView.setVisibility(View.VISIBLE);
                                    }
                                } catch (NullPointerException e) {
                                    Log.e("onResponse: ", e.toString());
                                    Toast.makeText(MainActivity.this, "The connection failed, press GO to retry", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<ResponseObject> call, @NonNull Throwable t) {
                                Log.e("onFailure: ", t.toString());
                                Toast.makeText(MainActivity.this, "The connection failed, press GO to retry", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            case 2:
                retrofit.houseListPrice(getString(R.string.city_name), maxPrice)
                        .enqueue(new Callback<ResponseObject>() {
                            @Override
                            public void onResponse(@NonNull Call<ResponseObject> call, @NonNull Response<ResponseObject> response) {
                                try {
                                    if (response.isSuccessful()) {
                                        List<House> houseList = response.body().getResponse().getListings();
                                        mAdapter[0] = new HousingListAdapter(houseList, MainActivity.this);
                                        mRecyclerView.setAdapter(mAdapter[0]);
                                        progressBar.setVisibility(View.INVISIBLE);
                                        mRecyclerView.setVisibility(View.VISIBLE);
                                    }
                                } catch (NullPointerException e) {
                                    Log.e("onResponse: ", e.toString());
                                    Toast.makeText(MainActivity.this, "The connection failed, press GO to retry", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<ResponseObject> call, @NonNull Throwable t) {
                                Log.e("onFailure: ", t.toString());
                                Toast.makeText(MainActivity.this, "The connection failed, press GO to retry", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            case 3:
                retrofit.houseListBoth(getString(R.string.city_name), bedroomNumber, maxPrice)
                        .enqueue(new Callback<ResponseObject>() {
                            @Override
                            public void onResponse(@NonNull Call<ResponseObject> call, @NonNull Response<ResponseObject> response) {
                                try {
                                    if (response.isSuccessful()) {
                                        List<House> houseList = response.body().getResponse().getListings();
                                        mAdapter[0] = new HousingListAdapter(houseList, MainActivity.this);
                                        mRecyclerView.setAdapter(mAdapter[0]);
                                        progressBar.setVisibility(View.INVISIBLE);
                                        mRecyclerView.setVisibility(View.VISIBLE);
                                    }
                                } catch (NullPointerException e) {
                                    Log.e("onResponse: ", e.toString());
                                    Toast.makeText(MainActivity.this, "The connection failed, press GO to retry", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<ResponseObject> call, @NonNull Throwable t) {
                                Log.e("onFailure: ", t.toString());
                                Toast.makeText(MainActivity.this, "The connection failed, press GO to retry", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            default:
                retrofit.houseListDefault( getString(R.string.city_name))
                        .enqueue(new Callback<ResponseObject>() {
                            @Override
                            public void onResponse(@NonNull Call<ResponseObject> call, @NonNull Response<ResponseObject> response) {
                                try {
                                    if (response.isSuccessful()) {
                                        List<House> houseList = response.body().getResponse().getListings();
                                        mAdapter[0] = new HousingListAdapter(houseList, MainActivity.this);
                                        mRecyclerView.setAdapter(mAdapter[0]);
                                        progressBar.setVisibility(View.INVISIBLE);
                                        mRecyclerView.setVisibility(View.VISIBLE);
                                    }
                                } catch (NullPointerException e) {
                                    Log.e("onResponse: ", e.toString());
                                    Toast.makeText(MainActivity.this, "The connection failed, press GO to retry", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<ResponseObject> call, @NonNull Throwable t) {
                                Log.e("onFailure: ", t.toString());
                                Toast.makeText(MainActivity.this, "The connection failed, press GO to retry", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
        }
    }

    @Override
    public void onListInteraction(House item) {
        Intent i = new Intent(MainActivity.this, DetailsActivity.class);
        i.putExtra("HOUSE", item);
        startActivity(i);
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radio_1:
                if (checked)
                    bedroomNumber = 1;
                break;
            case R.id.radio_2:
                if (checked)
                    bedroomNumber = 2;
                break;
            case R.id.radio_3:
                if (checked)
                    bedroomNumber = 3;
                break;
            case R.id.radio_4:
                if (checked)
                    bedroomNumber = 4;
                break;
        }
    }
}
