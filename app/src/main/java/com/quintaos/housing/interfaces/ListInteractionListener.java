package com.quintaos.housing.interfaces;

import com.quintaos.housing.models.House;

public interface ListInteractionListener {
    void onListInteraction(House item);
}
