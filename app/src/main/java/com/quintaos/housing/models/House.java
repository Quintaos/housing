package com.quintaos.housing.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class House implements Parcelable {

    public static final Creator<House> CREATOR = new Creator<House>() {
        @Override
        public House createFromParcel(Parcel in) {
            return new House(in);
        }

        @Override
        public House[] newArray(int size) {
            return new House[size];
        }
    };
    @SerializedName("thumb_url")
    private String thumbUrl;
    @SerializedName("keywords")
    private String keywords;
    @SerializedName("thumb_width")
    private String thumbWidth;
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("thumb_height")
    private String thumbHeight;
    @SerializedName("bathroom_number")
    private String bathroomNumber;
    @SerializedName("title")
    private String title;
    @SerializedName("bedroom_number")
    private String bedroomNumber;
    @SerializedName("price_low")
    private String priceLow;
    @SerializedName("updated_in_days_formatted")
    private String updatedInDaysFormatted;
    @SerializedName("lister_name")
    private String listerName;
    @SerializedName("location_accuracy")
    private String locationAccuracy;
    @SerializedName("price")
    private int price;
    @SerializedName("property_type")
    private String propertyType;
    @SerializedName("commission")
    private String commission;
    @SerializedName("datasource_name")
    private String datasourceName;
    @SerializedName("price_high")
    private String priceHigh;
    @SerializedName("price_currency")
    private String priceCurrency;
    @SerializedName("size_unit")
    private String sizeUnit;
    @SerializedName("longitude")
    private String longitude;
    @SerializedName("summary")
    private String summary;
    @SerializedName("img_width")
    private String imgWidth;
    @SerializedName("size_type")
    private String sizeType;
    @SerializedName("car_spaces")
    private String carSpaces;
    @SerializedName("img_height")
    private String imgHeight;
    @SerializedName("listing_type")
    private String listingType;
    @SerializedName("updated_in_days")
    private String updatedInDays;
    @SerializedName("lister_url")
    private String listerUrl;
    @SerializedName("price_formatted")
    private String priceFormatted;
    @SerializedName("construction_year")
    private String constructionYear;
    @SerializedName("size")
    private String size;
    @SerializedName("img_url")
    private String imgUrl;

    private House(Parcel in) {
        thumbUrl = in.readString();
        keywords = in.readString();
        thumbWidth = in.readString();
        latitude = in.readString();
        thumbHeight = in.readString();
        bathroomNumber = in.readString();
        title = in.readString();
        bedroomNumber = in.readString();
        priceLow = in.readString();
        updatedInDaysFormatted = in.readString();
        listerName = in.readString();
        locationAccuracy = in.readString();
        price = in.readInt();
        propertyType = in.readString();
        commission = in.readString();
        datasourceName = in.readString();
        priceHigh = in.readString();
        priceCurrency = in.readString();
        sizeUnit = in.readString();
        longitude = in.readString();
        summary = in.readString();
        imgWidth = in.readString();
        sizeType = in.readString();
        carSpaces = in.readString();
        imgHeight = in.readString();
        listingType = in.readString();
        updatedInDays = in.readString();
        listerUrl = in.readString();
        priceFormatted = in.readString();
        constructionYear = in.readString();
        size = in.readString();
        imgUrl = in.readString();
    }

    public String getPriceFormatted() {
        return priceFormatted;
    }

    public String getImgUrl() {

        return imgUrl;
    }

    public String getBathroomNumber() {
        if (bathroomNumber.equals(""))
            return "n/a";
        else
            return bathroomNumber;
    }

    public String getKeywords() {
        return keywords;
    }

    public String getBedroomNumber() {
        if (bedroomNumber.equals(""))
            return "n/a";
        else
            return bedroomNumber;
    }

    public String getTitle() {
        return title;
    }

    public String getSummary() {
        return summary;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(thumbUrl);
        parcel.writeString(keywords);
        parcel.writeString(thumbWidth);
        parcel.writeString(latitude);
        parcel.writeString(thumbHeight);
        parcel.writeString(bathroomNumber);
        parcel.writeString(title);
        parcel.writeString(bedroomNumber);
        parcel.writeString(priceLow);
        parcel.writeString(updatedInDaysFormatted);
        parcel.writeString(listerName);
        parcel.writeString(locationAccuracy);
        parcel.writeInt(price);
        parcel.writeString(propertyType);
        parcel.writeString(commission);
        parcel.writeString(datasourceName);
        parcel.writeString(priceHigh);
        parcel.writeString(priceCurrency);
        parcel.writeString(sizeUnit);
        parcel.writeString(longitude);
        parcel.writeString(summary);
        parcel.writeString(imgWidth);
        parcel.writeString(sizeType);
        parcel.writeString(carSpaces);
        parcel.writeString(imgHeight);
        parcel.writeString(listingType);
        parcel.writeString(updatedInDays);
        parcel.writeString(listerUrl);
        parcel.writeString(priceFormatted);
        parcel.writeString(constructionYear);
        parcel.writeString(size);
        parcel.writeString(imgUrl);
    }
}