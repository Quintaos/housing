package com.quintaos.housing.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("listings")
    private List<House> listings;

    public List<House> getListings() {
        return listings;
    }

}