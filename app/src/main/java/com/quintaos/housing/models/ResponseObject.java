package com.quintaos.housing.models;

import com.google.gson.annotations.SerializedName;

public class ResponseObject {

	@SerializedName("response")
	private Response response;

	public Response getResponse(){
		return response;
	}
}