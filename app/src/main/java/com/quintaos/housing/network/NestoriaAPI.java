package com.quintaos.housing.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quintaos.housing.models.ResponseObject;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NestoriaAPI {

    String BASE_URL = "https://api.nestoria.co.uk/";

    Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create();

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

    @GET("api?encoding=json&pretty=1&action=search_listings&country=uk")
    Call<ResponseObject> houseListDefault(@Query("place_name") String place_name);

    @GET("api?encoding=json&pretty=1&action=search_listings&country=uk")
    Call<ResponseObject> houseListPrice(@Query("place_name") String place_name,
                                        @Query("price_max") String price_max);

    @GET("api?encoding=json&pretty=1&action=search_listings&country=uk")
    Call<ResponseObject> houseListBedroom(@Query("place_name") String place_name,
                                          @Query("bedroom_max") int bedroom_max);

    @GET("api?encoding=json&pretty=1&action=search_listings&country=uk")
    Call<ResponseObject> houseListBoth(@Query("place_name") String place_name,
                                       @Query("bedroom_max") int bedroom_max,
                                       @Query("price_max") String price_max);
}
