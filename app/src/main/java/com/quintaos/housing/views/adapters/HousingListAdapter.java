package com.quintaos.housing.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.quintaos.housing.R;
import com.quintaos.housing.interfaces.ListInteractionListener;
import com.quintaos.housing.models.House;

import java.util.List;

public class HousingListAdapter extends RecyclerView.Adapter<HousingListAdapter.ViewHolder> {

    private final Context mContext;
    private final ListInteractionListener mListener;
    private List<House> houseList;

    public HousingListAdapter(List<House> items, Context context) {
        houseList = items;
        mListener = (ListInteractionListener) context;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_house, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = houseList.get(position);
        holder.mTitle.setText(holder.mItem.getTitle());
        holder.mSummary.setText(holder.mItem.getSummary());
        holder.mPrice.setText(holder.mItem.getPriceFormatted());

        Glide.with(mContext)
                .load(holder.mItem.getImgUrl())
                .centerCrop()
                .into(holder.mPicture);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return houseList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final ImageView mPicture;
        final TextView mTitle;
        final TextView mSummary;
        final TextView mPrice;

        House mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mPicture = view.findViewById(R.id.iv_picture);
            mTitle = view.findViewById(R.id.tv_title);
            mSummary = view.findViewById(R.id.tv_summary);
            mPrice = view.findViewById(R.id.tv_price);
        }

    }
}
